const output = document.getElementById('output');

const selectionSort = (array) => {
  if (array.length <= 1) {
    return array;
  }
  
  for (let i = 0; i < array.length - 1; i++) {
    let min = i;

    for (let j = i; j < array.length; j++) {
      if (array[j] < array[min]) {
        min = j;
      }
    }

    let temp = array[i];
    array[i] = array[min];
    array[min] = temp;
  }

  return array;
}

output.innerText = selectionSort(array = [1, 4, 2, 345, 123, 43, 32, 5643, 63, 123, 43, 2, 55, 1, 234, 92, 3]);
